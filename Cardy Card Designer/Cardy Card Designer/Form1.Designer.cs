﻿namespace Cardy_Card_Designer
{
    partial class CCD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CCD));
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameInput = new System.Windows.Forms.TextBox();
            this.rarityLabel = new System.Windows.Forms.Label();
            this.rarityInput = new System.Windows.Forms.TextBox();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.descriptionInput = new System.Windows.Forms.TextBox();
            this.artBox = new System.Windows.Forms.TextBox();
            this.artLabel = new System.Windows.Forms.Label();
            this.paramTable = new System.Windows.Forms.DataGridView();
            this.nameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueDCOL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxDCOL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.visibleDCOL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paramLabel = new System.Windows.Forms.Label();
            this.submitButton = new System.Windows.Forms.Button();
            this.classLabel = new System.Windows.Forms.Label();
            this.classInput = new System.Windows.Forms.TextBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.scriptsLabel = new System.Windows.Forms.Label();
            this.foundLocLabel = new System.Windows.Forms.Label();
            this.findScriptsBtn = new System.Windows.Forms.Button();
            this.dbLabel = new System.Windows.Forms.Label();
            this.foundDBLabel = new System.Windows.Forms.Label();
            this.findDBBtn = new System.Windows.Forms.Button();
            this.clearArtBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.paramTable)).BeginInit();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(6, 15);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(38, 13);
            this.nameLabel.TabIndex = 1;
            this.nameLabel.Text = "Name:";
            // 
            // nameInput
            // 
            this.nameInput.Location = new System.Drawing.Point(98, 15);
            this.nameInput.Name = "nameInput";
            this.nameInput.Size = new System.Drawing.Size(177, 20);
            this.nameInput.TabIndex = 1;
            this.nameInput.TextChanged += new System.EventHandler(this.NameInput_TextChanged);
            // 
            // rarityLabel
            // 
            this.rarityLabel.AutoSize = true;
            this.rarityLabel.Location = new System.Drawing.Point(6, 54);
            this.rarityLabel.Name = "rarityLabel";
            this.rarityLabel.Size = new System.Drawing.Size(37, 13);
            this.rarityLabel.TabIndex = 3;
            this.rarityLabel.Text = "Rarity:";
            // 
            // rarityInput
            // 
            this.rarityInput.Location = new System.Drawing.Point(98, 54);
            this.rarityInput.Name = "rarityInput";
            this.rarityInput.Size = new System.Drawing.Size(177, 20);
            this.rarityInput.TabIndex = 2;
            this.rarityInput.TextChanged += new System.EventHandler(this.RarityInput_TextChanged);
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(6, 221);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(63, 13);
            this.descriptionLabel.TabIndex = 6;
            this.descriptionLabel.Text = "Description:";
            // 
            // descriptionInput
            // 
            this.descriptionInput.Location = new System.Drawing.Point(98, 221);
            this.descriptionInput.Multiline = true;
            this.descriptionInput.Name = "descriptionInput";
            this.descriptionInput.Size = new System.Drawing.Size(177, 99);
            this.descriptionInput.TabIndex = 4;
            this.descriptionInput.TextChanged += new System.EventHandler(this.DescriptionInput_TextChanged);
            // 
            // artBox
            // 
            this.artBox.AllowDrop = true;
            this.artBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.artBox.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.artBox.Location = new System.Drawing.Point(309, 23);
            this.artBox.MaxLength = 500;
            this.artBox.Multiline = true;
            this.artBox.Name = "artBox";
            this.artBox.Size = new System.Drawing.Size(372, 245);
            this.artBox.TabIndex = 5;
            this.artBox.Text = resources.GetString("artBox.Text");
            this.artBox.TextChanged += new System.EventHandler(this.ArtBox_TextChanged);
            // 
            // artLabel
            // 
            this.artLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.artLabel.AutoSize = true;
            this.artLabel.Location = new System.Drawing.Point(472, 7);
            this.artLabel.Name = "artLabel";
            this.artLabel.Size = new System.Drawing.Size(45, 13);
            this.artLabel.TabIndex = 9;
            this.artLabel.Text = "Card Art";
            // 
            // paramTable
            // 
            this.paramTable.AllowUserToResizeColumns = false;
            this.paramTable.AllowUserToResizeRows = false;
            this.paramTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.paramTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.paramTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameCol,
            this.valueDCOL,
            this.maxDCOL,
            this.visibleDCOL});
            this.paramTable.Location = new System.Drawing.Point(80, 329);
            this.paramTable.Name = "paramTable";
            this.paramTable.RowHeadersWidth = 82;
            this.paramTable.Size = new System.Drawing.Size(427, 156);
            this.paramTable.TabIndex = 10;
            this.paramTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ParamTable_CellContentClick);
            // 
            // nameCol
            // 
            this.nameCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.nameCol.HeaderText = "name";
            this.nameCol.MinimumWidth = 10;
            this.nameCol.Name = "nameCol";
            this.nameCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.nameCol.Width = 58;
            // 
            // valueDCOL
            // 
            this.valueDCOL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.valueDCOL.HeaderText = "value_default";
            this.valueDCOL.MinimumWidth = 10;
            this.valueDCOL.Name = "valueDCOL";
            this.valueDCOL.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.valueDCOL.Width = 96;
            // 
            // maxDCOL
            // 
            this.maxDCOL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.maxDCOL.HeaderText = "max_default";
            this.maxDCOL.MinimumWidth = 10;
            this.maxDCOL.Name = "maxDCOL";
            this.maxDCOL.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.maxDCOL.Width = 89;
            // 
            // visibleDCOL
            // 
            this.visibleDCOL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.visibleDCOL.HeaderText = "visible_default";
            this.visibleDCOL.MinimumWidth = 10;
            this.visibleDCOL.Name = "visibleDCOL";
            this.visibleDCOL.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.visibleDCOL.Width = 99;
            // 
            // paramLabel
            // 
            this.paramLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.paramLabel.AutoSize = true;
            this.paramLabel.Location = new System.Drawing.Point(6, 398);
            this.paramLabel.Name = "paramLabel";
            this.paramLabel.Size = new System.Drawing.Size(60, 26);
            this.paramLabel.TabIndex = 11;
            this.paramLabel.Text = "Card\r\nParameters";
            // 
            // submitButton
            // 
            this.submitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.submitButton.Location = new System.Drawing.Point(523, 417);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(158, 68);
            this.submitButton.TabIndex = 12;
            this.submitButton.Text = "Submit Card";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // classLabel
            // 
            this.classLabel.AutoSize = true;
            this.classLabel.Location = new System.Drawing.Point(6, 92);
            this.classLabel.Name = "classLabel";
            this.classLabel.Size = new System.Drawing.Size(66, 13);
            this.classLabel.TabIndex = 13;
            this.classLabel.Text = "Class Name:";
            // 
            // classInput
            // 
            this.classInput.Location = new System.Drawing.Point(98, 92);
            this.classInput.Name = "classInput";
            this.classInput.Size = new System.Drawing.Size(177, 20);
            this.classInput.TabIndex = 3;
            this.classInput.TextChanged += new System.EventHandler(this.ClassInput_TextChanged);
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteButton.Location = new System.Drawing.Point(523, 329);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(158, 68);
            this.deleteButton.TabIndex = 15;
            this.deleteButton.Text = "Delete Row";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // scriptsLabel
            // 
            this.scriptsLabel.AutoSize = true;
            this.scriptsLabel.Location = new System.Drawing.Point(6, 176);
            this.scriptsLabel.Name = "scriptsLabel";
            this.scriptsLabel.Size = new System.Drawing.Size(86, 13);
            this.scriptsLabel.TabIndex = 16;
            this.scriptsLabel.Text = "Scripts Location:";
            // 
            // foundLocLabel
            // 
            this.foundLocLabel.AutoSize = true;
            this.foundLocLabel.Location = new System.Drawing.Point(95, 176);
            this.foundLocLabel.Name = "foundLocLabel";
            this.foundLocLabel.Size = new System.Drawing.Size(35, 13);
            this.foundLocLabel.TabIndex = 17;
            this.foundLocLabel.Text = "<null>";
            // 
            // findScriptsBtn
            // 
            this.findScriptsBtn.Location = new System.Drawing.Point(98, 192);
            this.findScriptsBtn.Name = "findScriptsBtn";
            this.findScriptsBtn.Size = new System.Drawing.Size(177, 23);
            this.findScriptsBtn.TabIndex = 18;
            this.findScriptsBtn.Text = "Find Directory";
            this.findScriptsBtn.UseVisualStyleBackColor = true;
            this.findScriptsBtn.Click += new System.EventHandler(this.FindScriptsBtn_Click);
            // 
            // dbLabel
            // 
            this.dbLabel.AutoSize = true;
            this.dbLabel.Location = new System.Drawing.Point(6, 123);
            this.dbLabel.Name = "dbLabel";
            this.dbLabel.Size = new System.Drawing.Size(69, 13);
            this.dbLabel.TabIndex = 19;
            this.dbLabel.Text = "DB Location:";
            // 
            // foundDBLabel
            // 
            this.foundDBLabel.AutoSize = true;
            this.foundDBLabel.Location = new System.Drawing.Point(95, 123);
            this.foundDBLabel.Name = "foundDBLabel";
            this.foundDBLabel.Size = new System.Drawing.Size(35, 13);
            this.foundDBLabel.TabIndex = 20;
            this.foundDBLabel.Text = "<null>";
            // 
            // findDBBtn
            // 
            this.findDBBtn.Location = new System.Drawing.Point(98, 140);
            this.findDBBtn.Name = "findDBBtn";
            this.findDBBtn.Size = new System.Drawing.Size(177, 23);
            this.findDBBtn.TabIndex = 21;
            this.findDBBtn.Text = "Find DB";
            this.findDBBtn.UseVisualStyleBackColor = true;
            this.findDBBtn.Click += new System.EventHandler(this.FindDBBtn_Click);
            // 
            // clearArtBtn
            // 
            this.clearArtBtn.Location = new System.Drawing.Point(404, 274);
            this.clearArtBtn.Name = "clearArtBtn";
            this.clearArtBtn.Size = new System.Drawing.Size(177, 46);
            this.clearArtBtn.TabIndex = 22;
            this.clearArtBtn.Text = "Clear Art";
            this.clearArtBtn.UseVisualStyleBackColor = true;
            this.clearArtBtn.Click += new System.EventHandler(this.ClearArtBtn_Click);
            // 
            // CCD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(693, 497);
            this.Controls.Add(this.clearArtBtn);
            this.Controls.Add(this.findDBBtn);
            this.Controls.Add(this.foundDBLabel);
            this.Controls.Add(this.dbLabel);
            this.Controls.Add(this.findScriptsBtn);
            this.Controls.Add(this.foundLocLabel);
            this.Controls.Add(this.scriptsLabel);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.classInput);
            this.Controls.Add(this.classLabel);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.paramLabel);
            this.Controls.Add(this.paramTable);
            this.Controls.Add(this.artLabel);
            this.Controls.Add(this.artBox);
            this.Controls.Add(this.descriptionInput);
            this.Controls.Add(this.descriptionLabel);
            this.Controls.Add(this.rarityInput);
            this.Controls.Add(this.rarityLabel);
            this.Controls.Add(this.nameInput);
            this.Controls.Add(this.nameLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CCD";
            this.Text = "3";
            this.Load += new System.EventHandler(this.CCD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.paramTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameInput;
        private System.Windows.Forms.Label rarityLabel;
        private System.Windows.Forms.TextBox rarityInput;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.TextBox descriptionInput;
        private System.Windows.Forms.TextBox artBox;
        private System.Windows.Forms.Label artLabel;
        private System.Windows.Forms.DataGridView paramTable;
        private System.Windows.Forms.Label paramLabel;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Label classLabel;
        private System.Windows.Forms.TextBox classInput;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueDCOL;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxDCOL;
        private System.Windows.Forms.DataGridViewTextBoxColumn visibleDCOL;
        private System.Windows.Forms.Label scriptsLabel;
        private System.Windows.Forms.Label foundLocLabel;
        private System.Windows.Forms.Button findScriptsBtn;
        private System.Windows.Forms.Label dbLabel;
        private System.Windows.Forms.Label foundDBLabel;
        private System.Windows.Forms.Button findDBBtn;
        private System.Windows.Forms.Button clearArtBtn;
    }
}

