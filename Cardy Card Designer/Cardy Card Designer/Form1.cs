﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Input;
using System.IO;
using System.Text;

namespace Cardy_Card_Designer
{
    public partial class CCD : Form
    {
        SQLiteConnection cardDB;

        public CCD()
        {
            InitializeComponent();
            //after we start the program, immediately log into db file
        }

        //this program simply just finds the DB and logs into it
        void connectToDB()
        {
            cardDB = new SQLiteConnection("Data Source=" + foundDBLabel.Text + ";Version=3");
            cardDB.Open();
        }

        //ths function will take the data in the datagrid rows and put them in the query string to add to the DB
        void fillParamType()
        {
            //first, we need to collect the card id
            string query = "SELECT last_insert_rowid();";

            //next, we create a command to go and extract said card id
            SQLiteCommand retrieveID = new SQLiteCommand(query, cardDB);
            
            //we create a variable to house that informaton
            var data = retrieveID.ExecuteScalar();

            //loop through each row in each column
            foreach (DataGridViewRow row in paramTable.Rows)
            {
                
                if (row.Cells[0].Value != null)
                {
                    //add to the query the data from the cells and the card id from before
                    query += "insert into ParamType (name, value_default, max_default, visible_default, card_type) values (\"" +
                    row.Cells[0].Value + "\",\"" +
                    row.Cells[1].Value + "\",\"" +
                    row.Cells[2].Value + "\",\"" +
                    row.Cells[3].Value + "\",\"" +
                    data.ToString() + "\");";
                }
            }
            
            //make final command from it
            SQLiteCommand sendIT = new SQLiteCommand(query, cardDB);

            //execute order 66
            sendIT.ExecuteNonQuery();
        }

        //this function will take text from the textboxes and form the query with them to send to the DB
        void fillCardType()
        {
            //we create the string that will hold the wuery with its values to submit
            string query = "insert into CardType (name, rarity, description, art, class_name) values (\"" +
            nameInput.Text +
            "\",\"" + rarityInput.Text +
            "\",\"" + descriptionInput.Text +
            "\",\"" + artBox.Text +
            "\",\"" + classInput.Text + "\");"; 

            //we create the command to execute
            SQLiteCommand sendIT = new SQLiteCommand(query, cardDB);

            //we execute the command
            sendIT.ExecuteNonQuery();
        }
        
        //this function will create a py script for every card created
        void createPyClass()
        {
            
                //we create the path and name of file for this card
                string pyScript = foundLocLabel.Text + "/" + classInput.Text + ".py";

                //we check if this file already exists
                if (File.Exists(pyScript))
                {
                    //if it does, we rename the new file semi-iteratively
                    pyScript = foundLocLabel.Text + classInput.Text + "_1.py";
                }

                //we create the text to file the script with
                //the uncomfortable text formatting is left this way on purpose, so that it looks programmatically clean in the py script
                string pyText = @"import card

class " + classInput.Text + @"(Card):
    def use(self, message) -> str:
        raise NotImplementedError
    
    def passive(self, message, t) -> str:
        raise NotImplementedError
";

                //we create the file in this function
                using (FileStream fs = File.Create(pyScript))
                {
                    //we store the text into a byte array and write to the file
                    Byte[] info = new UTF8Encoding(true).GetBytes(pyText);
                    fs.Write(info, 0, info.Length);
                }
            
        }

        //we call the functions here when submit is presed
        private void SubmitButton_Click(object sender, EventArgs e)
        {
            //we wanna make sure the card submitted has the correct format before submitting
            if (artBox.Text.Length != 492)
            {
                MessageBox.Show("Card Art incorrect Legnth");
                return;
            }

            if (foundDBLabel.Text == "<null>")
            {
                MessageBox.Show("No Database selected");
                return;
            }

            if (foundLocLabel.Text == "<null>")
            {
                MessageBox.Show("Please choose a directory for saving cards");
                return;
            }

            //if it does meet the formatting correct, we send it
            fillCardType();
            fillParamType();
            createPyClass();

            //let the user know their card has been submit
            MessageBox.Show("Card sucessfully submitted!");
        }

        private void ClassInput_TextChanged(object sender, EventArgs e)
        {

        }

        //this will delete selected rows by the user
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = paramTable.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                for (int i = 0; i < selectedRowCount; i++)
                {
                    paramTable.Rows.RemoveAt(paramTable.SelectedRows[0].Index);
                }
            }
        }

        

        //This is for finding the location for saving the py scripts
        private void FindScriptsBtn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog findDirectory = new FolderBrowserDialog();

            if (findDirectory.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foundLocLabel.Text = findDirectory.SelectedPath;
            }
        }

        //This is for finding the database file you want to use
        private void FindDBBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDB = new OpenFileDialog();

            openDB.Filter = "DB Files|*.db";
            if (openDB.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foundDBLabel.Text = openDB.SafeFileName;
                connectToDB();
            }
        }

        //This button resets the card art box to its default format
        //Yeah, i dont like the way this looks either, but it needs to be formatted like this in code, so that it looks right in the program
        private void ClearArtBtn_Click(object sender, EventArgs e)
        {
            artBox.Text = @"/----------------------------------\
|                                  |
|                                  |
|                                  |
|                                  |
|                                  |
|                                  |
|                                  |
|                                  |
|                                  |
|                                  |
|                                  |
\----------------------------------/
";
        }

        //These empty functions just exist for reference;
        //WPF is annoying and doesnt like it when these are deleted
        private void NameInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void RarityInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void DescriptionInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void ArtBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void ParamTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CCD_Load(object sender, EventArgs e)
        {

        }
    }
}
